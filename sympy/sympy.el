(defun sympy-evaluate ()
  "Calcola un'espressione usando sympy restituendo codice LaTeX."
  (interactive)
  (setq debug-on-error t)

  (let ((match nil))
    (if (search-forward-regexp "sympy\\(.*?\\)sympyend" nil t)
        (setq match (match-string 1))
      (if (search-backward-regexp "sympy\\(.*?\\)sympyend" nil t)
            (setq match (match-string 1))))
    (if match
        (progn
          (replace-match
           (shell-command-to-string
            (concat user-emacs-directory
                    "sympy/calculate.py \""
                    match "\""))
           t t)))))
