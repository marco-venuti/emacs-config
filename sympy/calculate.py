#!/usr/bin/env python3
from sympy import *
from sys import argv

match = argv[1]

x, y, z, t = symbols('x y z t')
k, m, n = symbols('k m n', integer=True)
f, g, h = symbols('f g h', cls=Function)

init_printing()
risultato = eval('latex(' + match.replace('\\', '')
                 .replace('^', '**')
                 .replace('{', '(')
                 .replace('}', ')') + ')')

print(risultato)
