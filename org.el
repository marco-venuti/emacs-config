;; packages
(use-package org
  :bind (:map org-mode-map
         ("C-M-<right>" . org-metaright)
         ("C-M-<left>" . org-metaleft)
         ("C-M-<up>" . org-metaup)
         ("C-M-<down>" . org-metadown)
         ("ESC ESC ESC" . org-overview)
         ("C-n" . (lambda () (interactive) (org-insert-math nil)))
         ("C-S-n" . (lambda () (interactive) (org-insert-math t)))
         ("<f5>" . (lambda () (interactive) (save-buffer) (org-latex-export-to-pdf))))
  :config
  (setq org-startup-folded nil
      org-agenda-files '("~/org/agenda")
      org-default-notes-file (concat org-directory "/agenda/random.org")
      org-agenda-span 14
      org-enforce-todo-dependencies t
      org-enforce-todo-checkbox-dependencies t
      org-log-done 'time
      org-ellipsis "⤵"
      org-capture-templates '(("t" "Todo item" entry (file "")
                               "* TODO %?\nAdded %U\n %i")
                              ("s" "Todo with schedule" entry (file "")
                               "* TODO %?\nSCHEDULED: %t\nAdded %U\n %i")
                              ("d" "Todo with deadline" entry (file "")
                               "* TODO %?\nDEADLINE: %t\nAdded %U\n %i")
                              ("u" "Todo for university" entry (file "~/org/agenda/uni.org")
                               "* TODO %?\nSCHEDULED: %t\nAdded %U\n %i")
                              ("j" "Journal entry" entry (file+datetree "~/org/journal.org")
                               "* %?\nAdded %U\n %i"))
      org-src-fontify-natively t
      org-src-tab-acts-natively t
      org-src-window-setup 'current-window
      org-latex-tables-booktabs t
      org-highlight-latex-and-related '(latex script entities)
      org-latex-default-packages-alist
      '(("utf8" "inputenc" t ("pdflatex"))
        ("T1" "fontenc" t ("pdflatex"))
        ("" "graphicx" t)
        ("" "grffile" t)
        ("" "longtable" nil)
        ("" "wrapfig" nil)
        ("" "rotating" nil)
        ("normalem" "ulem" t)
        ("" "amsmath" t)
        ("" "amssymb" t)
        ("" "textcomp" t)
        ("" "capt-of" nil)
        ("bookmarks,colorlinks,linkcolor=blue" "hyperref" nil))
       )

  :hook
  (org-mode . (lambda () (setq-local electric-pair-pairs (append electric-pair-pairs
                                                                 '((?* . ?*) (?/ . ?/) (?~ . ?~) ;; (?= . ?=) (?_ . ?_) (?+ . ?+)
                                                                   )))))
  )

(use-package org-bullets
  :ensure t
  :init
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode t))))

(use-package ox-twbs :ensure t)
(use-package htmlize :ensure t)
(use-package poly-org :ensure t)

;; hooks
(add-hook 'org-mode-hook (lambda () (toggle-truncate-lines -1)))

;; global keybindings
(bind-keys*
 ("M-a" . org-agenda-list)
 ("C-c c" . org-capture))

;; set html and bootstrap export templates and options
(setq org-twbs-mathjax-template
"<script>
 window.MathJax = {
     svg: {
         displayAlign: \"%ALIGN\",
         displayIndent: \"%INDENT\",
         scale: %SCALE
     },
     chtml: {
         scale: %SCALE
     },
     options: {
         ignoreHtmlClass: 'tex2jax_ignore',
         processHtmlClass: 'tex2jax_process'
     },
     tex: {
         autoload: {
             color: [],
             colorV2: ['color']
         },
         packages: {'[+]': ['noerrors']},
         macros: {
             dod: ['\\\\dfrac{\\\\mathrm d #1}{\\\\mathrm d #2}', 2],
             dpd: ['\\\\dfrac{\\\\partial #1}{\\\\partial #2}', 2],
             dif: ['\\\\mathrm{d} #1', 1],
             del: ['\\\\left(#1\\\\right)', 1],
             sbr: ['\\\\left[#1\\\\right]', 1],
             cbr: ['\\\\left\\\\{#1\\\\right\\\\}', 1],
             vec: ['\\\\mathbf{#1}', 1],
             abs: ['\\\\left|#1\\\\right|', 1],
             ped: ['_{\\\\text{#1}}', 1],
             coloneqq: [':=', 0]
         }
     },
     loader: {
         load: ['[tex]/noerrors']
     }
 };
</script>
<script src=\"%PATH\" id=\"MathJax-script\"></script>")

(setq org-twbs-mathjax-options
      '((path "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js")
        (scale "1")
        (dscale "1")
        (align "center")
        (indent "2em")
        (messages "none")))

(setq org-html-mathjax-template org-twbs-mathjax-template
      org-html-mathjax-options org-twbs-mathjax-options)

(defun org-insert-math (display)
  (let
      ((open (if display "\\[ " "\\( "))
       (close (if display " \\]" " \\)"))
       (shift (if display 3 3)))
    (if (use-region-p)
        (let
            ((end (region-end)))
          (save-excursion
            (goto-char (region-beginning))
            (insert open)
            (goto-char (+ end shift))
            (insert close)))
      (progn
        (insert (concat open close))
        (backward-char shift)))))

;; open the agenda at startup
(org-agenda-list)
