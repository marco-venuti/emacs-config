(package-initialize)
(require 'package)

;; remove useless stuff from the GUI
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)

;; set theme
(if (package-installed-p 'doom-themes)
    (progn
      (load-theme 'doom-one t)
      (set-background-color "#121419")))

;; Add repositories
(add-to-list
 'package-archives
 '("melpa" . "https://melpa.org/packages/"))

;; Set information about me
(setq user-full-name "Marco Venuti"
      user-mail-address "afm.itunev@gmail.com")

;; Install use-package unless already present
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))
(setq use-package-always-ensure t)

;; IMPORTS

;; custom variables file
(setq custom-file (concat user-emacs-directory "custom.el"))
(if (file-exists-p custom-file) (load custom-file))

;; start emacs server mode
(server-start)

;; load sympy stuff
(load-file (concat user-emacs-directory "sympy/sympy.el"))

;; PACKAGES

(use-package auto-package-update
  :config
  (setq auto-package-update-delete-old-versions t
        auto-package-update-hide-results t
        auto-package-update-interval 10
        auto-package-update-prompt-before-update t)
  (auto-package-update-maybe))

(load-file (concat user-emacs-directory "latex.el"))
(load-file (concat user-emacs-directory "org.el"))

(use-package align
  :config
  (add-to-list 'align-exclude-rules-list '((regexp . "\\(\\s-*\\)\\\\\\\\")
                                           (modes . align-tex-modes))))

(use-package elpy
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable)
  (advice-add 'python-mode :after 'activate-current-venv)
  :config
  (setq python-shell-interpreter "python3"
        elpy-rpc-python-command "python3"
        elpy-modules (delq 'elpy-module-flymake elpy-modules)))

(use-package yasnippet
  :defer t
  :config
  (yas-global-mode 1)
  (setq yas-triggers-in-field t))

(use-package yasnippet-snippets :defer t)
(use-package markdown-mode)
(use-package nginx-mode)
(use-package jinja2-mode)
(use-package crontab-mode)
(use-package yaml-mode
  :config
  (add-hook 'yaml-mode-hook 'highlight-indentation-current-column-mode)
  (setq highlight-indentation-blank-lines t))

(use-package dockerfile-mode)
(use-package haskell-mode)
(use-package php-mode)
(use-package arduino-mode)
(use-package apache-mode)
(use-package rust-mode)
(use-package gnuplot)
(use-package ini-mode)
(use-package rainbow-mode)
(use-package julia-mode)
(use-package systemd)
(use-package generic-x) ;; https://www.emacswiki.org/emacs/GenericMode
(use-package maxima)
(use-package wolfram
  :config
  (setq wolfram-indent 4)
  (add-to-list 'auto-mode-alist '("\\.m\\|\\.nb\\'" . wolfram-mode))
  )
(use-package wolfram-mode)
(use-package apt-sources-list)
(use-package kubernetes)
(use-package k8s-mode)
(use-package doom-themes)
(use-package doom-modeline
  :config
  (doom-modeline-mode))
(use-package all-the-icons)
(use-package graphviz-dot-mode
  :config
  (setq graphviz-dot-indent-width 4))

(use-package multiple-cursors
  :bind (("C-S-c C-S-c" . mc/edit-lines)
         ("C-S-c >" . mc/mark-next-like-this)
         ("C-S-c <" . mc/mark-previous-like-this)
         ("C-S-c C-S-a" . mc/mark-all-like-this)))


(use-package crux
  :bind (("C-k" . crux-smart-kill-line)
         ("C-c D" . crux-delete-file-and-buffer)
         ;; ("C-c D" . crux-copy-file-preserve-attributes)
         ("C-d" . crux-duplicate-current-line-or-region)
         ("C-S-d" . crux-duplicate-and-comment-current-line-or-region)
         ("C-c r" . crux-rename-file-and-buffer)
         ("C-c i" . crux-find-user-init-file)))

(use-package beacon
  :config
  (beacon-mode 1))

(use-package undo-tree
  :config
  (global-undo-tree-mode)
  (bind-keys*
   ("C-x u" . undo-tree-undo)
   ("C-x C-u" . undo-tree-visualize)))

(use-package flycheck
  :config
  (add-hook 'typescript-mode-hook 'flycheck-mode)
  (add-hook 'TeX-mode 'flycheck-mode)
  (add-hook 'LaTeX-mode-hook 'flycheck-mode)
  (add-hook 'haskell-mode-hook 'flycheck-mode)
  (add-hook 'python-mode-hook 'flycheck-mode)
  (add-hook 'c-mode-hook 'flycheck-mode)
  (add-hook 'f90-mode-hook 'flycheck-mode)
  (setq
   flycheck-python-flake8-executable
   (concat user-emacs-directory "elpy/rpc-venv/bin/flake8")
   flycheck-flake8-maximum-line-length 150
   flycheck-gfortran-language-standard "f2008"))

(use-package company
  :config
  (setq company-idle-delay 0
        company-minimum-prefix-length 2
        company-show-numbers t
        company-tooltip-align-annotations t
        company-tooltip-flip-when-above t)
  (global-company-mode)
  (bind-key* "C-M-i" 'company-complete))

(use-package company-quickhelp
  :init
  (company-quickhelp-mode 1)
  (use-package pos-tip))

(use-package company-auctex
  :init
  (company-auctex-init))

(use-package company-reftex ;; non funziona...
  :config
  (add-to-list 'company-backends 'company-reftex-labels)
  (add-to-list 'company-backends 'company-reftex-citations))

(use-package company-math
  :config
  ;; (setq company-math-allow-unicode-symbols-in-faces nil)
  (add-to-list 'company-backends 'company-math-symbols-unicode))

(use-package company-arduino :ensure t)

(use-package web-mode
  :mode (("\\.html?\\'" . web-mode)
         ("\\.tsx\\'" . web-mode)
         ("\\.jsx\\'" . web-mode))
  :config
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-block-padding 2
        web-mode-comment-style 2

        web-mode-enable-css-colorization t
        web-mode-enable-auto-pairing t
        web-mode-enable-comment-keywords t
        web-mode-enable-current-element-highlight t)
  (setq web-mode-content-types-alist '(("django" . "\\.html\\")))
  (flycheck-add-mode 'typescript-tslint 'web-mode))

(use-package typescript-mode
  :config
  (setq typescript-indent-level 2)
  (add-hook 'typescript-mode 'subword-mode))

(use-package flyspell
  :config
  (setq ispell-program-name "hunspell"
        ispell-dictionary "it_IT,en_US")
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "it_IT,en_US")
  (set-face-attribute
   'flyspell-incorrect nil
   :underline '(:color "#ff0000" :style wave)
   :background "#4c0000"
   )
  (add-hook 'org-mode-hook 'flyspell-mode)
  (add-hook 'latex-mode-hook 'flyspell-prog-mode)
  (add-hook 'python-mode 'flyspell-prog-mode)
  (bind-keys*
   ("M-s s" . ispell-buffer)
   ("M-s f" . flyspell-buffer)))

(use-package irony
  :config
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

(use-package flycheck-irony
  :config
  (eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup)))

(use-package expand-region
  :bind (("C-+" . er/expand-region)))

;; RANDOM CONFIG

;; default tab width
(setq-default indent-tabs-mode nil)
(setq default-tab-width 4)

;; C
(setq
 c-default-style "linux"
 c-basic-offset 4)

;; autosave directory
(setq backup-directory-alist '(("." . "~/.emacs-saves")))

;; font size
(set-face-attribute 'default nil :height 140)

(setq inhibit-splash-screen t)

;; dired
(setq wdired-allow-to-change-permissions t
      wdired-allow-to-redirect-links t
      wdired-use-interactive-rename t)

(add-hook 'wdired-mode-hook 'undo-tree-mode)

;; GLOBAL HOOKS
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Move in newly created window
(advice-add
 'split-window-right
 :after
 '(lambda (dummy) (other-window 1)))

(advice-add
 'split-window-below
 :after
 '(lambda (dummy) (other-window 1)))


;; GLOBAL KEY BINDINGS
(global-set-key (kbd "C-T") 'comment-line) ;; toggle comment
(global-set-key (kbd "<f9>") 'window-swap-states) ;; swap windows
(global-set-key (kbd "C-S-<down-mouse-1>") 'mouse-start-rectangle) ;; mouse rectangle mark mode
(global-set-key (kbd "C-M-S-<up>") 'move-region-up)
(global-set-key (kbd "C-M-S-<down>") 'move-region-down)

(global-set-key (kbd "C-x C-b") 'ibuffer)

(bind-key* "C-<tab>" 'other-window)
(bind-key* "C-S-k" 'kill-unsaved)
(bind-key* "C-S-r" 'revert-buffer)
(bind-key* "C-x C-r" 'crux-eval-and-replace)

;; delete words instead of killing them
(bind-keys*
 ("<C-delete>" . delete-word)
 ("<C-backspace>" . backward-delete-word)
 ("M-DEL" . backward-delete-word))

;; Move around windows with M-<arrows>
(bind-keys*
 ("M-<left>" . windmove-left)
 ("M-<right>" . windmove-right)
 ("M-<up>" . windmove-up)
 ("M-<down>" . windmove-down))


;; GLOBAL MODES AND VARIABLES
(defalias 'yes-or-no-p 'y-or-n-p)

;; brackets
(electric-pair-mode 1)

(show-paren-mode 1)
(setq show-paren-delay 0
      show-paren-when-point-inside-paren t
      show-paren-when-point-in-periphery t)

;; show function name
(which-function-mode 1)

;; cursor
(setq-default cursor-type '(bar . 4))
(set-cursor-color "#33f702")

;; highlight current line
(global-hl-line-mode t)
(set-face-background 'hl-line "#252525")
(set-face-foreground 'highlight nil)

;; other stuff
(semantic-mode 1)
(setq gdb-show-main t)
(winner-mode 1)
(delete-selection-mode 1)

;; tramp
(setq tramp-ssh-controlmaster-options
      "-o ControlMaster=auto -o ControlPath='~/.ssh_sockets/tramp.%%C' -o ControlPersist=no")

(fset 'macro-tramp-ssh
      (kmacro-lambda-form [?\C-x ?\C-f ?\S-\C-a backspace ?/ ?s ?s ?h ?: ?\C-u ?\C-x ?q] 0 "%d"))
(bind-key* "C-u C-x C-f" 'macro-tramp-ssh)
(setq auth-source-save-behavior nil)

;; minibuffer history
(savehist-mode 1)

;; scrolling
(setq scroll-step 1
      mouse-wheel-scroll-amount '(1 ((shift) . 10))
      mouse-wheel-progressive-speed nil
      scroll-preserve-screen-position t)

(put 'scroll-left 'disabled nil)

(setq recenter-positions '(middle 5 -5))


;; RANDOM FUNCTIONS
(defun company-yasnippet-or-completion ()
  "Solve company yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
         (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
          (lambda ()
            (substitute-key-definition
             'company-complete-common
             'company-yasnippet-or-completion
             company-active-map)))

(defun activate-current-venv ()
  (if
      (locate-dominating-file "." "venv")
      (let
          ((venv-path (concat (locate-dominating-file "." "venv") "venv")))
        (pyvenv-activate venv-path)
        (message (concat "Activate virtual environment: " venv-path)))
      (message "No virtual environment found.")))

(defun mouse-start-rectangle (start-event)
  (interactive "e")
  (deactivate-mark)
  (mouse-set-point start-event)
  (rectangle-mark-mode +1)
  (let ((drag-event))
    (track-mouse
      (while (progn
               (setq drag-event (read-event))
               (mouse-movement-p drag-event))
        (mouse-set-point drag-event)))))

(defun move-region (start end n)
  "Move the current region up or down by N lines."
  (interactive "r\np")
  (let ((line-text (delete-and-extract-region start end)))
    (forward-line n)
    (let ((start (point)))
      (insert line-text)
      (setq deactivate-mark nil)
      (set-mark start))))

(defun move-region-up (start end n)
  "Move the current line up by N lines."
  (interactive "r\np")
  (move-region start end (if (null n) -1 (- n))))

(defun move-region-down (start end n)
  "Move the current line down by N lines."
  (interactive "r\np")
  (move-region start end (if (null n) 1 n)))

(defun kill-unsaved ()
  "Kills all unmodified buffers."
  (interactive)
  (dolist (buffer (buffer-list))
    (and (buffer-live-p buffer)
         (not (buffer-modified-p buffer))
         (kill-buffer buffer)))
  (delete-other-windows))

(defun delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument, do this that many times."
  (interactive "p")
  (if (use-region-p)
      (delete-region (region-beginning) (region-end))
    (delete-region (point) (progn (forward-word arg) (point)))))

(defun backward-delete-word (arg)
  "Delete characters backward until encountering the end of a word.
With argument, do this that many times."
  (interactive "p")
  (delete-word (- arg)))
