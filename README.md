# Configurazione di Emacs
In teoria la configurazione di Emacs dovrebbe essere tale da autoinstallare tutti i pacchetti richiesti. Al primo avvio potrebbe impiegare parecchio tempo per installare tutto. Al termine delle operazioni riavviare Emacs e controllare che non dia errori. Una volta al giorno Emacs chiede (all'avvio) se si vuole che i pacchetti aggiornabili vengano aggiornati.

## Virtualenv
Elpy necessita di un suo virtualenv, che si crea automaticamente in `~/.emacs.d/elpy/rpc-venv` (se non esiste già) installandosi i pacchetti necessari.
È però anche necessario che Emacs attivi il virtualenv del repo su cui si sta lavorando. Per fare questo, quando si apre un file `.py` Emacs cerca una directory `venv` risalendo la gerarchia partendo dal file corrente.
