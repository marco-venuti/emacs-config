(defun insert-math (display)
  (let
      ((open (if display "\\[ " "\\( "))
       (close (if display " \\]" " \\)"))
       (shift (if display 3 3)))
    (if (use-region-p)
        (let
            ((end (region-end)))
          (save-excursion
            (goto-char (region-beginning))
            (insert open)
            (goto-char (+ end shift))
            (insert close)))
      (progn
        (insert (concat open close))
        (backward-char shift)))))

(defun begin-end-regexps (env)
  (list env
        (replace-regexp-in-string "{\\([a-zA-Z]+\\)\\*}" "{\\1\\\\*}" (concat "\\\\begin{" env "}\\([^\0]*?\\)\\\\end{" env "}"))
        (concat "\\\\begin{" env "}\\1\\\\end{" env "}")))

(defun loop-math-modes (step)
  (if (texmathp)
      (let
          ((math-modes (list '("$" "\\$\\([^$\0]+?\\)\\$" "$\\1$")
                             '("\\(" "\\\\(\\([^\0]*?\\)\\\\)" "\\\\(\\1\\\\)")
                             '("\\[" "\\\\\\[\\([^\0]*?\\)\\\\\\]" "\\\\[\\1\\\\]")
                             (begin-end-regexps "equation")
                             (begin-end-regexps "equation*")
                             (begin-end-regexps "align")
                             (begin-end-regexps "align*")
                             (begin-end-regexps "gather")
                             (begin-end-regexps "gather*")
                             (begin-end-regexps "multline")
                             (begin-end-regexps "multline*")))
           commands)

        (dolist (element math-modes) ;; get list of cars
          (setq commands
                (append commands (list (car element)))))
        (let
            ((math-mode-index (cl-position (car texmathp-why) commands :test 'equal))
             (current-pos (point)))
            (goto-char (cdr texmathp-why)) ;; for some reason save-excursion is not working
            (re-search-forward (nth 1 (nth math-mode-index math-modes)))
            (replace-match (nth 2 (nth (mod (+ math-mode-index step) (length math-modes)) math-modes)))
            (backward-char 1)))
    (message "Not inside math environment")))

(defun my-LaTeX-compile ()
  (interactive)
  (let* ((TeX-command-force "LaTeX")
         (master (TeX-master-file))
         (process (and (stringp master) (TeX-process master))))
    (when (and (processp process) (eq (process-status process) 'run))
      (delete-process process))
    (save-buffer)
    (TeX-command "LaTeX" 'TeX-master-file)))

(defun my-LuaTeX-compile ()
  (interactive)
  (save-buffer)
  (start-process
   "lualatex-compile"
   buffer-file-name
   "lualatex"
   "-interaction=nonstopmode" "-shell-escape" "--synctex=1"
   buffer-file-name)
  (message "Compilo..."))

(use-package tex
  :ensure auctex
  :bind (:map TeX-mode-map
              ("C-C s" . sympy-evaluate)
              ("\"" . tex-insert-quote)
              ("$" . self-insert-command)
              ("C-n" . (lambda () (interactive) (insert-math nil)))
              ("C-S-n" . (lambda () (interactive) (insert-math t)))
              ("M-n" . (lambda () (interactive) (loop-math-modes 1)))
              ("M-N" . (lambda () (interactive) (loop-math-modes -1)))
              ("C-v" . TeX-view)
              ("C-c C-c" . my-LaTeX-compile)
              ("C-c C-l" . my-LuaTeX-compile)
              ;; ("<tab>" . outline-cycle)
              )
  :config
  (setq TeX-auto-save t
        TeX-parse-self t
        TeX-electric-sub-and-superscript t
        LaTeX-electric-left-right-brace t
        TeX-view-program-selection '((output-pdf "Evince"))
        TeX-source-correlate-start-server t
        word-wrap t
        LaTeX-indent-level 4
        reftex-plug-into-AUCTeX t)
  (TeX-source-correlate-mode t)
  (add-hook 'TeX-after-compilation-finished-functions
            'TeX-revert-document-buffer)
  (add-hook 'TeX-mode-hook (lambda ()
                               (setq TeX-master (guess-TeX-master (buffer-file-name))))))

(defun guess-TeX-master (filename)
  "Guess the master file for filename from currently open .tex files."
  (let ((candidate nil)
        (filename (file-name-sans-extension (file-name-nondirectory filename))))
    (save-excursion
      (dolist (buffer (buffer-list))
        (with-current-buffer buffer
          (let ((name (buffer-name))
                (file buffer-file-name))
            (if (and file (string-match "\\.tex$" file))
                (progn
                  (goto-char (point-min))
                  (if (re-search-forward (concat "\\\\input{.*?" filename ".*?}") nil t)
                      (setq candidate file))
                  (if (re-search-forward (concat "\\\\include{.*?" filename ".*?") nil t)
                      (setq candidate file))))))))
    (if candidate
        (message "TeX master document: %s" (file-name-nondirectory candidate)))
    candidate))

(defun decentify ()
  (interactive)
  (save-excursion
    (untabify 0 (buffer-size))
    (delete-trailing-whitespace)
    (beginning-of-buffer)
    (push-mark)
    (setq mark-active t)
    (end-of-buffer)
    (indent-for-tab-command)
    (deactivate-mark)))
